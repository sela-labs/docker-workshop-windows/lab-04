# Docker Workshop - Windows
Lab 04: Docker Networks

---

## Instructions

 - Display all the existent networks in the host:
```
$ docker network ls
```

 - Let's create a new bridge network:
```
$ docker network create -d nat my-nat-network
```

 - Run the application container linked to the created network:
```
$ docker run -d --name win-app --network=my-nat-network selaworkshops/windows-app
```

 - Find the container internal ip using:
```
$ docker inspect win-app
```
```
"Networks": {
                "my-bridge-network": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": [
                        "cf148e899ea8"
                    ],
                    "NetworkID": "82057fd5c45f1d62b0a96ac905af529ef6715e9f1038e6307f83b6698717dcfa",
                    "EndpointID": "24c5ac91c33c089427cf2f81cdc09c3ee4db09ecc0cb3b8298ba223689c10f4a",
                    "Gateway": "172.21.0.1",
                    "IPAddress": "172.21.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:15:00:02",
                    "DriverOpts": null
                }
            }
```

 - Open a new terminal and run a windows container in interactive mode:
```
$ docker run -it --name windows mcr.microsoft.com/windows/servercore:ltsc2019
```

 - From the windows container try to browse to the win-app container:
```
$ curl <win-app-container-internal-ip>:80
```

 - From other terminal session (host terminal) run the command below to attach the windows container to the created network:
```
$ docker network connect my-nat-network windows
```

 - From the windows container terminal try to browse to the app container again:
```
$ curl <win-app-container-internal-ip>:80
```

 - Now you will be able to reach the application:

 - Inspect the network from the host terminal and look for the linked containers:
```
$ docker inspect my-nat-network
```
```
        "Containers": {
            "3b719cc996b3a03d828984e763a6ed83d7c4fcdfd52e9133a13183eec79b203c": {
                "Name": "windows",
                "EndpointID": "c2cd94e246b4bcd3ea60f80a1ae717ed5f1a9121001c5189a541cb06a3291ca7",
                "MacAddress": "00:15:5d:7c:00:30",
                "IPv4Address": "172.28.46.115/16",
                "IPv6Address": ""
            },
            "ca9f2f86955fc519bbbfceaf6da6ae6a81e18b15d68a78e4695707d1891464c2": {
                "Name": "win-app",
                "EndpointID": "04fbbb9002c70dda2cc12025940e2f36eb14847afaa24478ba9db3c3a285df7e",
                "MacAddress": "00:15:5d:7c:09:8f",
                "IPv4Address": "172.28.46.237/16",
                "IPv6Address": ""
            }
        },
```

 - Disconnect both containers from the created network (regular terminal):
```
$ docker network disconnect my-nat-network win-app
$ docker network disconnect my-nat-network windows
```

 - Delete the network:
```
$ docker network rm my-nat-network
```

 - Ensure that the network was deleted:
```
$ docker network ls
```

 - Exit from the windows container and close the terminal:
```
$ exit
```

## Cleanup

 - Remove used containers:
```
$ docker rm -f $(docker ps -a -q)
```